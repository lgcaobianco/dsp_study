#include <stdio.h>
#include <math.h>
#include <iostream>

const int tamanho_sinal = 4096;


int calcula_haar(float *vetor_onda){
	using namespace std;
	float h[2], g[2], sinal_filtrado[tamanho_sinal];
	int percorre_sinal_filtrado, i;
	cout<<"calculo da TW Haar" << endl;
	h[0] = 1/(sqrt(2));
	h[1] = 1/(sqrt(2));
	g[0] = 1/(sqrt(2));
	g[1] = -1/(sqrt(2));

	//filtragem h
	for(i=0; i<tamanho_sinal; i = i+2){
		sinal_filtrado[percorre_sinal_filtrado] =  ((vetor_onda[i] * h[0]) + (vetor_onda[i+1] * h[1]));
		percorre_sinal_filtrado++;
	}

	//filtragem g
	for(i=1; i<tamanho_sinal; i= i+2){
		sinal_filtrado[percorre_sinal_filtrado] =  ((vetor_onda[i] * g[0]) + (vetor_onda[i+1] * g[1]));
		percorre_sinal_filtrado++;

	}

	FILE *arquivo_saida = fopen("dwt_haar.csv", "w+");
	for(i=0; i<tamanho_sinal; i++){
		fprintf(arquivo_saida, "%f\n", sinal_filtrado[i]);
	}

	fclose(arquivo_saida);
	cout << "Fim haar" <<endl;
	return 1;
}

int calcula_db(float *vetor_onda){
	using namespace std;
	int suporte_filtro, i=0, percorre_sinal_filtrado=0;

	cout<<"insira qual o suporte dos filtros" <<endl;
	scanf("%d", &suporte_filtro);

	float sinal_filtrado[tamanho_sinal];
	float  h[suporte_filtro], g[suporte_filtro];
	
	FILE *le_filtros, *arquivo_saida;
	le_filtros = fopen("db_h", "r");

	while( (fscanf(le_filtros, "%f\n", &h[i]) ) != EOF ){
		i++;
	}
	fclose(le_filtros);

	fopen("db_g", "r");
	i=0;

	while( (fscanf(le_filtros, "%f\n", &g[i]) ) != EOF ){
		i++;
	}
	fclose(le_filtros);

	cout<<"escolha 0 para ver os filtros, qualquer outro valor para calcular a db." <<endl;
	int menu;
	scanf("%d", &menu);
	
	if(menu == 0){
		cout <<"filtro h: " <<endl;
		for(int i=0; i< suporte_filtro; i++){
			cout << h[i] << ", ";
		} 
		cout<<""<<endl;


		cout <<"filtro g: " <<endl;
		for(int i=0; i< suporte_filtro; i++){
			cout << g[i] << ", ";
		} 
		cout<<""<<endl;
	}

	else{
		for(int i=0; i<tamanho_sinal; i++){
			sinal_filtrado[i] =0;
		}
		//filtragem h
		for(i=0; i<tamanho_sinal; i = i+2){
			for(int aux=0; aux<suporte_filtro; aux++){
				
				int total = aux + i;
				if(  total >= tamanho_sinal){
					int posicao = i+aux-tamanho_sinal;
					sinal_filtrado[percorre_sinal_filtrado] += vetor_onda[posicao] * h[aux];	
				} 
				if(total < tamanho_sinal){
					sinal_filtrado[percorre_sinal_filtrado] += vetor_onda[i+aux] * h[aux];
				}
			}
			percorre_sinal_filtrado++;
		}
		cout<<"fim da filtragem h"<< endl;
		cout<<"O valor de percorre_sinal_filtrado é: " <<percorre_sinal_filtrado <<endl;
		
		//filtragem g
		for(i=1; i<tamanho_sinal; i= i+2){
			for(int aux=0; aux<suporte_filtro; aux++){
				int total = aux + i;
				if(total >= tamanho_sinal){
					int posicao = i+aux-tamanho_sinal;
					printf("%d\n", posicao);
					sinal_filtrado[percorre_sinal_filtrado] += vetor_onda[posicao] * g[aux];	
				} 
				if(total<tamanho_sinal){
					sinal_filtrado[percorre_sinal_filtrado] += vetor_onda[i+aux] * g[aux];
				}
			}
			percorre_sinal_filtrado++;
		}

		cout<< "fim da filtragem de g" <<endl;
		arquivo_saida = fopen("dwt_db.csv", "w+");
		for(int i=0; i<tamanho_sinal; i++){
			fprintf(arquivo_saida, "%f\n", sinal_filtrado[i]);
		}

		cout<<"Fim da impressao de arquivos" << endl;
	}	
	return 1;
}

int main(){
	FILE *arquivo_entrada;
	using namespace std;

	int i=0, menu;
	float vetor_onda[tamanho_sinal];

	arquivo_entrada = fopen("dados_onda.csv", "r");
	while( (fscanf(arquivo_entrada, "%f\n", &vetor_onda[i]) ) != EOF ){
		i++;
	}

	//	cout << "Fim da leitura do arquivo de entrada" <<endl;
	fclose(arquivo_entrada);

	cout<<"Escolha 0 para sair, 1 para Haar, 2 para Daubechies" << endl;
	scanf("%d", &menu);
	switch(menu){
		case 1: calcula_haar(vetor_onda);
				break;

		case 2: calcula_db(vetor_onda);
				break;


		default: return 1;
	};
}


