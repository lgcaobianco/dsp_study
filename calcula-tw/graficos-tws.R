dwt_db = read.csv("dwt_db.csv")
db_y = dwt_db[,1];
db_y = db_y / max(db_y)
sinal_entrada = read.csv("dados_onda.csv")

y_sinal = sinal_entrada[,1]
plot(rownames(sinal_entrada), y_sinal, type = "l", bty = "l", axes=F, ylab = "Amplitude", xlab = "Amostra" )
axis(2, pos=0, at=seq(-2, 2, by=0.5))
axis(1, pos=0, at=seq(0, 4200, by=600))
lines(rownames(dwt_db), db_y, col="red")
par(fig = c(0, 1, 0, 1), oma = c(0, 0, 0, 0), mar = c(0, 0, 0, 0), new = TRUE)
legend("bottom",legend=c("TW Db4", "Sinal Original"),
       col=c("red", "black"), lty=1:1, cex=0.8)


plot(rownames(sinal_entrada), db_y, type = "l", bty = "l", axes=F, 
     ylab = "Amplitude", xlab = "Amostra", main  ="Comparação entre TW Haar e Db4")

dwt_haar = read.csv("dwt_haar.csv")
haar_y = dwt_haar[,1]
haar_y = haar_y/max(haar_y)
lines(rownames(dwt_haar), haar_y, col="green")

plot(rownames(sinal_entrada), haar_y)