\changetocdepth {4}
\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {brazil}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdu\c {c}\~{a}o}}{6}{chapter.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Estado da Arte}}{7}{chapter.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Fundamenta\IeC {\c c}\IeC {\~a}o Te\IeC {\'o}rica}}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}O Princ\IeC {\'\i }pio da Incerteza de Heinsenberg}{9}{section.3.1}
\contentsline {section}{\numberline {3.2}A Transformada Wavelet}{9}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}A Transformada Wavelet Cont\IeC {\'\i }nua}{10}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}A Transformada Wavelet Discreta}{10}{subsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.1}Abordagem Pr\IeC {\'a}tica}{11}{subsubsection.3.2.2.1}
\contentsline {subsubsection}{\numberline {3.2.2.2}Abordagem Te\IeC {\'o}rica}{11}{subsubsection.3.2.2.2}
\contentsline {subsection}{\numberline {3.2.3}A Teoria da Multirresolu\IeC {\c c}\IeC {\~a}o}{12}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Algoritmo de Mallat}{12}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Fam\IeC {\'\i }lias da DWT}{14}{subsection.3.2.5}
\contentsline {subsubsection}{\numberline {3.2.5.1}Haar}{15}{subsubsection.3.2.5.1}
\contentsline {subsubsection}{\numberline {3.2.5.2}Daubechies $n$}{16}{subsubsection.3.2.5.2}
\contentsline {subsubsection}{\numberline {3.2.5.3}Symmlets}{17}{subsubsection.3.2.5.3}
\contentsline {subsubsection}{\numberline {3.2.5.4}Coiflets}{18}{subsubsection.3.2.5.4}
\contentsline {section}{\numberline {3.3}Discuss\IeC {\~a}o das Fam\IeC {\'\i }lias DWT}{19}{section.3.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {M\IeC {\'e}todos Aplicados}}{20}{chapter.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Conclus\IeC {\~a}o}}{25}{chapter.5}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Bibliografia}{27}{section*.16}
