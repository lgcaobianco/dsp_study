#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  1 15:08:49 2017

@author: gudev
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import plotly.plotly  as py
from plotly.offline import offline
import plotly.graph_objs as go


dataset = pd.read_csv('onda_reconstruida.csv')
dataset2 = pd.read_csv('dados_onda.csv')
dataset3 = pd.read_csv('dft_total.csv')

x_transformada = dataset3.iloc[:, 1]
y_transformada = dataset3.iloc[:, 2]

x_values = dataset.iloc[:, 0]
y_values = dataset.iloc[:, 1]




x_onda = dataset2.iloc[:, 0]
y_onda = dataset2.iloc[:, 1]

bar1 = go.Bar(
        x = x_transformada,
        y = y_transformada,
        name = 'Transformada do sinal')


trace2 = go.Scatter(
        x = x_values,
        y = y_values,
        mode = 'lines',
        opacity = 0.5,
        name = 'Onda Reconstruida')

trace1 = go.Scatter(
        x = x_onda,
        y = y_onda,
        mode = 'lines',
        name = 'Onda Original')

layout1 = go.Layout(
        title = 'Comparação da onda original e onda reconstruída',
        xaxis = dict(title='Amostras',
                     titlefont=dict(
                             family='Arial, sans-serif',
                             size=18,
                             color='black')),
                     yaxis = dict(title='Amplitude',
                     titlefont=dict(
                             family='Arial, sans-serif',
                             size=18,
                             color='black')))
        



layout2 = go.Layout(
        title = 'Sinal de voz exemplo',
        xaxis = dict(title='Amostras',
                     titlefont=dict(
                             family='Arial, sans-serif',
                             size=18,
                             color='black')),
                     yaxis = dict(title='Frequência',
                     titlefont=dict(
                             family='Arial, sans-serif',
                             size=18,
                             color='black')))
        
data2 = [trace1, trace2]
fig2 = go.Figure(data=data2, layout = layout1)

py.iplot(fig2, filename='compara_onda_reconstruida')


width=0.25
fig, ax = plt.subplots()
rects1 = ax.bar(x_values, y_values, width, color='r')
plt.show()
