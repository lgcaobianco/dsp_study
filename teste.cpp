#include <stdio.h>
#include <math.h>
#include <string.h>
#include <iostream>
#include <thread>
#include <atomic>
#include <mutex>
// ==================
#define PI 3.14159265
#define RESET   "\033[0m"
#define RED     "\033[31m"
#define GREEN   "\x1b[32m"
#define BLUE    "\x1b[34m"
// ==================
void calcula_dft_inversa(double *signal, int inicio, int final, int M){
  printf(RED "INVERSA da DFT do sinal\n" RESET);
  double sinal_reconstruido[M], real[M], imaginario[M];
  int pedaco_inicial = inicio;
  char buf[0x100];
  snprintf(buf, sizeof(buf), "teste_onda_reconstruida_%i.csv", pedaco_inicial);
  FILE *escreve_saida = fopen(buf, "w+");
  fprintf(escreve_saida, "posicao,amplitude\n");  

  FILE *entrada;
  entrada = fopen("teste.csv", "r");

  int linha_arquivo =0, posicao;
  char lixo1, lixo2;

  //loop para importar todos os valores da dft p/ dentro do programa
  while( (fscanf(entrada, "%d %c %lf %c %lf\n", &posicao, &lixo1, &real[linha_arquivo], &lixo2, &imaginario[linha_arquivo])) != EOF){    
    linha_arquivo++;
  }
  //inicializa o sinal reconstruido com 0's
  for(int i=0; i<M; i++){
    sinal_reconstruido[i] = 0;
  }
	double soma_real=0, soma_imaginario=0, rad, angulo, dist1, dist2, dist3, dist4, total1, total2;
  while(inicio<final){
    for(int i=0; i<M; i++){
      angulo = ((2*180*inicio*i)/(M));
      rad = angulo *(PI/180);
      //printf("para o k=%i, o angulo sera: %lf e o valor da transformada usada e %lf real %lf img\n", inicio, angulo, real[i], imaginario[i]);
      
      dist1 = (real[i] * cos(rad));
      dist2 = (real[i] * sin(rad));
      dist3 = (imaginario[i] * cos(rad));
      dist4 = -(imaginario[i] * sin(rad));
      total1 = dist1 + dist4;
      total2 = dist2 + dist3;      

      soma_real += total1;
      soma_imaginario += total2; //faz a soma de todos os valores imaginarios de todas as amostras
       
    }
    sinal_reconstruido[inicio] = (soma_real + soma_imaginario)/M;
    fprintf(escreve_saida, "%lf\n", sinal_reconstruido[inicio]);
    soma_real=0;
    soma_imaginario=0;
    if( (inicio%1000) == 0){
      std::cout << BLUE << "RECONSTRUINDO SINAL" << RESET << " - Thread da parte " << RED << pedaco_inicial << RESET << " em: " <<inicio << " de: " << final <<std::endl;
     
    }

    if(inicio == (final-1)){
      std::cout <<"========================================" << std::endl;
      std::cout << GREEN << "A THREAD da parte " << inicio << " ACABOU!" << RESET << std::endl;
      std::cout <<"========================================" << std::endl;
    }

    inicio++;
    
  }

  FILE *saida = fopen("onda_reconstruida.csv", "w+");
  fprintf(saida, "posicao,amplitude");
  for(int i=0; i<M; i++){
    printf("%d,%f\n", i, sinal_reconstruido[i]);
  }

  fclose(saida);

}


void calcula_dft(double *signal, int inicio, int final, int M){
  double real[M], imaginario[M];
  double matriz_resposta[M][2];
  int pedaco_inicial = inicio;
  double soma_real=0, soma_imaginario=0;

  char buf[0x100];
  snprintf(buf, sizeof(buf), "teste.csv");
  FILE *escreve_saida = fopen(buf, "w+");
  
    printf(RED "DFT do sinal\n" RESET);
  //inicializa os vetores com 0. A posicao [i] de ambos os vetores representam a DFT do elemento i do vetor de amostras.
  for(int i=0; i<M; i++){
    real[i] = 0;
    imaginario[i] = 0;
  }

  
  double rad, angulo;
  while(inicio < final)
  {
    for(int i=0; i<M; i++){
		angulo = ((2*180*inicio*i)/(M));
		rad = angulo *(PI/180);
		soma_real += (signal[i] * cos(rad)); //faz a soma de todos os valores reais de todas as amostras
		soma_imaginario += (signal[i] * -sin(rad)); //faz a soma de todos os valores imaginarios de todas as amostras
		

    }

    fprintf(escreve_saida, "%i,%f,%f\n", inicio, soma_real, soma_imaginario);
	  printf("%lf,%lf\n", soma_real, soma_imaginario);
    //reaseta
     soma_real=0;
    soma_imaginario=0;
    inicio++;
    
  }
    

  fclose(escreve_saida);



}
// ==================

int main(){
	

	double s[4]={1, 2, 4, 4};
    printf(RED "sinal original\n" RESET);
  for(int i=0; i<4; i++)
    printf("%lf\n", s[i]);
  calcula_dft(s, 0, 4, 4);
  calcula_dft_inversa(s, 0, 4, 4);
}
    