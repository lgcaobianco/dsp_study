#include <stdio.h>
#include <math.h>
#include <cmath>

int main(){


	FILE *saida;
	double sinal[4096];

	saida = fopen("sinal_fabricado.csv", "w+");

	for(int i=0; i<4096; i++){
		if(i <1024){
			sinal[i] = 1.2 * (sin ((2*3.1415*44*i)/4095) * pow(  2.71828, -(pow(  (i/409.6),(2) ))));
		}
		if(i>=1024 && i<2048){
			sinal[i]=0;
		}

		if(i>=2048 && i<4096){
			sinal[i] = sin((2 * 3.1415 * 85 * i)/4095);
		}
	}

	fprintf(saida, "amplitude,frequencia\n");
	for(int i=0; i<4096; i++){
		fprintf(saida, "%d,%lf\n", i, sinal[i]);
	}


	return 1;




}