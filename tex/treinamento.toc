\changetocdepth {4}
\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {brazil}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdu\c {c}\~{a}o}}{6}{chapter.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Estado da Arte}}{7}{chapter.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Fundamenta\c {c}\~{a}o Te\'{o}rica}}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}Zero Crossing Rate}{11}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Sinais Unidimensionais}{11}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Entradas Bidimensionais}{14}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Energia}{14}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Sinais Unidimensionais}{14}{subsection.3.2.1}
\contentsline {section}{\numberline {3.3}Entropia}{15}{section.3.3}
\contentsline {section}{\numberline {3.4}Transformada Discreta de Fourier}{17}{section.3.4}
\contentsline {section}{\numberline {3.5}Amostragem}{18}{section.3.5}
\contentsline {section}{\numberline {3.6}Filtros}{18}{section.3.6}
\contentsline {section}{\numberline {3.7}Convolu\IeC {\c c}\IeC {\~a}o}{19}{section.3.7}
\contentsline {section}{\numberline {3.8}Filtro FIR}{20}{section.3.8}
\contentsline {section}{\numberline {3.9}Filtro IIR}{20}{section.3.9}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Os m\IeC {\'e}todos aplicados}}{22}{chapter.4}
\contentsline {section}{\numberline {4.1}Zero Crossing Rate}{22}{section.4.1}
\contentsline {section}{\numberline {4.2}Energia}{23}{section.4.2}
\contentsline {section}{\numberline {4.3}A Transformada Discreta de Fourier}{25}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}A Inversa da Transformada Discreta de Fourier}{27}{subsection.4.3.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Conclus\IeC {\~a}o}}{29}{chapter.5}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Bibliografia}{31}{section*.5}
