#include <stdio.h>
#include <math.h>
#include <string.h>
#include <iostream>
#include <thread>
#include <atomic>
#include <mutex>
// ==================
#define PI 3.14159265
#define RESET   "\033[0m"
#define RED     "\033[31m"
#define BLUE    "\x1b[34m"
#define GREEN   "\x1b[32m"
// ==================


/* message board ---------------------------------------------------
-------------PRECISO IMPLEMENTAR ENTROPIA E DFT!!! -----------------
------------------------------------------------------------------*/

//==================
const int L = 256; //tamanho janela
const int V = 50;  //overlapping
const int tamanho_espectro = 500;

//==================

//---------------------------------------

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//========================================

int main(int i,char* n[])
{
  void analisa_dados_brutos(double*,int);
  short converte2de8para1de16(unsigned char, unsigned char);


  FILE* fr;

  if(((fr=fopen(n[1],"rb"))!=NULL))
  {
    struct
    {
      unsigned char riff[4];
      unsigned int len;
    } riff_header;
    fread(&riff_header,sizeof(riff_header),1,fr);
    std::cout<<"\nArquivo do tipo: "<<riff_header.riff[0]<<riff_header.riff[1]<<riff_header.riff[2]<<riff_header.riff[3];
    std::cout<<"\nTamanho excluindo header: "<<riff_header.len;

        ///////////////////////////////////////////////////////////////////////////

    unsigned char wave[4];
        fread(&wave,sizeof(wave),1,fr); //////
        std::cout<<"\nSub-Tipo: "<<wave[0]<<wave[1]<<wave[2]<<wave[3];

        ///////////////////////////////////////////////////////////////////////////
        struct
        {
          unsigned char id[4];
          unsigned int len;
        } riff_chunk;
        fread(&riff_chunk,sizeof(riff_chunk),1,fr);
        std::cout<<"\nIdentificador: "<<riff_chunk.id[0]<<riff_chunk.id[1]<<riff_chunk.id[2]<<riff_chunk.id[3];
        std::cout<<"\nComprimento do chunk apos header: "<<riff_chunk.len;

        //////////////////////////////////////////////////////////////////////////////
        struct
        {
          unsigned short formattag;
          unsigned short numberofchannels;
          unsigned int samplingrate;
          unsigned int avgbytespersecond;
          unsigned short blockalign;
        }  wave_chunk;
        fread(&wave_chunk,sizeof(wave_chunk),1,fr);

//tratamento de uma excessao que costuma aparecer em alguns arquivos wav... O correto seriam 16 bytes, as vezes aparecem 18 ou mais...
        if(riff_chunk.len>16)
        {
         unsigned char excesso;
         for(int i=0;i<riff_chunk.len-16;i++)
          fread(&excesso,sizeof(excesso),1,fr);
      }
	//fim do tratamento da excess?
      std::cout<<"\nCategoria do formato: "<<wave_chunk.formattag;
      std::cout<<"\nNumero de canais: "<<wave_chunk.numberofchannels;
      std::cout<<"\nTaxa de amostragem: "<<wave_chunk.samplingrate;
      std::cout<<"\nMedia do num. de bps: "<<wave_chunk.avgbytespersecond;
      std::cout<<"\nAlinhamento do bloco em bytes: "<<wave_chunk.blockalign;

        //////////////////////////////////////////////////////////////////////////////

        if(wave_chunk.formattag==1) //PCM
        {
                int resolucao=(wave_chunk.avgbytespersecond * 8)/(wave_chunk.numberofchannels * wave_chunk.samplingrate);// pq nao bitssample
                std::cout<<"\nResolucao: "<<resolucao;

                struct
                {
                  unsigned char data[4];
                  unsigned int chunk_size;
                } header_data_chunk;

                fread(&header_data_chunk,sizeof(header_data_chunk),1,fr);
                std::cout<<"\nIdentificacao: "<<header_data_chunk.data[0]<<header_data_chunk.data[1]<<header_data_chunk.data[2]<<header_data_chunk.data[3];
                std::cout<<"\nTamanho do chunk de dados: "<<header_data_chunk.chunk_size;
                std::cout<<"\nNumero de frames para amostrar: "<<header_data_chunk.chunk_size/wave_chunk.blockalign;

                int tamanho_da_janela=header_data_chunk.chunk_size/wave_chunk.blockalign;

                std::cout<<"\nTamanho da janela: "<<tamanho_da_janela<<std::endl;
                if((resolucao==8) && (wave_chunk.numberofchannels==1))
                {
                 unsigned char waveformdata;
                 double* amostras_no_tempo = new double[tamanho_da_janela];
                 for(int i=0;i<tamanho_da_janela;i++)
                 {
                   fread(&waveformdata,sizeof(waveformdata),1,fr);
                   amostras_no_tempo[i]=(double)waveformdata;
                 }

                 analisa_dados_brutos(&amostras_no_tempo[0],tamanho_da_janela);
               }
               else if((resolucao==8) && (wave_chunk.numberofchannels==2))
               {
                 unsigned char waveformdata_right;
                 unsigned char waveformdata_left;
                 double* amostras_no_tempo_left = new double[tamanho_da_janela];
                 double* amostras_no_tempo_right = new double[tamanho_da_janela];
                 for(int i=0;i<tamanho_da_janela;i++)
                 {
                   fread(&waveformdata_left,sizeof(waveformdata_left),1,fr);
                   fread(&waveformdata_right,sizeof(waveformdata_right),1,fr);

                   amostras_no_tempo_right[i]=(double)waveformdata_right;
                   amostras_no_tempo_left[i]=(double)waveformdata_left;
                 }

                 analisa_dados_brutos(&amostras_no_tempo_left[0],tamanho_da_janela);
                 analisa_dados_brutos(&amostras_no_tempo_right[0],tamanho_da_janela);
               }
               else if((resolucao==16) && (wave_chunk.numberofchannels==1))
               {
                 unsigned char waveformdata_lsb, waveformdata_msb;
                 double* amostras_no_tempo = new double[tamanho_da_janela];
                 for(int i=0;i<tamanho_da_janela;i++)
                 {
                   fread(&waveformdata_lsb,sizeof(waveformdata_lsb),1,fr);
                   fread(&waveformdata_msb,sizeof(waveformdata_msb),1,fr);
                   amostras_no_tempo[i]=(double)converte2de8para1de16(waveformdata_lsb,waveformdata_msb);
                 }

                 analisa_dados_brutos(&amostras_no_tempo[0],tamanho_da_janela);
               }
               else if ((resolucao==16) && (wave_chunk.numberofchannels==2))
               {
                 unsigned char waveformdata_lsb_left, waveformdata_lsb_right, waveformdata_msb_left, waveformdata_msb_right;
                 double* amostras_no_tempo_left = new double[tamanho_da_janela];
                 double* amostras_no_tempo_right = new double[tamanho_da_janela];
                 for(int i=0;i<tamanho_da_janela;i++)
                 {
                   fread(&waveformdata_lsb_left,sizeof(waveformdata_lsb_left),1,fr);
                   fread(&waveformdata_msb_left,sizeof(waveformdata_msb_left),1,fr);
                   fread(&waveformdata_lsb_right,sizeof(waveformdata_lsb_right),1,fr);
                   fread(&waveformdata_msb_right,sizeof(waveformdata_msb_right),1,fr);
                   amostras_no_tempo_left[i]=(double)converte2de8para1de16(waveformdata_lsb_left,waveformdata_msb_left);
                   amostras_no_tempo_right[i]=(double)converte2de8para1de16(waveformdata_lsb_right,waveformdata_msb_right);
                 }

                 analisa_dados_brutos(&amostras_no_tempo_left[0],tamanho_da_janela);
                 analisa_dados_brutos(&amostras_no_tempo_right[0],tamanho_da_janela);
               }
               else
                std::cout<<"Resolucao ou nmero de canais invalido(s)";

            }
            else
              std::cout<<"FORA DO FORMATO PCM...";
            fclose(fr);
          }
          else
            std::cout<<"Arquivo nao existe ou nao pode ser aberto";
          std::cout<<"\n\n\n";
        }
//--------------------------------------------------------
        short converte2de8para1de16(unsigned char lsb, unsigned char msb)
        {
          return(((msb&0x80)>>7)*(32768) +
           ((msb&0x40)>>6)*(16384) +
           ((msb&0x20)>>5)*(8192) +
           ((msb&0x10)>>4)*(4096) +
           ((msb&0x08)>>3)*(2048) +
           ((msb&0x04)>>2)*(1024) +
           ((msb&0x02)>>1)*(512) +
           ((msb&0x01))*(256) +
           ((lsb&0x80)>>7)*(128) +
           ((lsb&0x40)>>6)*(64) +
           ((lsb&0x20)>>5)*(32) +
           ((lsb&0x10)>>4)*(16) +
           ((lsb&0x08)>>3)*(8) +
           ((lsb&0x04)>>2)*(4) +
           ((lsb&0x02)>>1)*(2) +
           (lsb&0x01));
        }
//-------------------------------------------------------
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        void analisa_dados_brutos(double* s, int M){
          using namespace std;
          double *signal = s;

          int verifica_media_sinal(double*,int);

  //==== metodos ZCR ====
          double *zcr_metodo1_b1(double*,int);
          double *zcr_metodo2_b1(double*,int);
          double *zcr_metodo3_b1(double*,int);

          double *zcr_metodo1_b2(double*, int);
          double *zcr_metodo2_b2(double*, int); 
          double *zcr_metodo3_b2(double*, int);
          double *zcr_metodo1_b3(double*, int);
  //=====================

  //==== metodos energia ====
          double *energia_metodo1(double*, int);
          double *energia_metodo2(double*, int);
          double *energia_metodo3(double*, int);
  //==========================

  //Discrete Fourier Transform
          void calcula_dft(double *, int, int, int);
          void calcula_dft_inversa(double *, int, int, int);
  //==========================

  //== funcoes talvez uteis ==
          void imprime_onda(double*, int);
          void escala_log(double*, int);
          void normaliza_vetor(double *, int);
  //==========================

  //== concatenacao d arqvs ==
          void concatena_arquivos_dft(int, int, int, int);
          void concatena_arquivos_inversa(int, int, int, int);
  //==========================          

  //== espectro de potencia ==
          void calcula_espectro_potencia(double *, int , int);
  //==========================



          //calcula_espectro_potencia(signal, M , tamanho_espectro);


  //== funcoes talvez uteis, descomentar e usar

  //escala_log(s, M);
  //==
          cout<<"imprimindo a aonda"<<endl;
          imprime_onda(signal, M);
          cout<<"fim"<<endl;


          cout<<"\n\n=================ENERGIA==================="<<endl;
          cout<<"== ENERGIA METODO 1                      =="<<endl;
          double *fe_vector1_energia = energia_metodo1(signal, M);
          cout<<"== FIM METODO 1                          =="<<endl;

          cout<<"== ENERGIA METODO 2                      =="<<endl;
          double *fe_vector2_energia = energia_metodo2(signal, M);
          cout<<"== FIM METODO 2                          =="<<endl;

          cout<<"== ENERGIA METODO 3                      =="<<endl;
          double *fe_vector3_energia = energia_metodo3(signal, M);
          cout<<"== FIM METODO 3                          =="<<endl;
          cout<<"=============== FIM ENERGIA ==============="<<endl;
          cout<<"\n\n\n\n"<<endl;


  //============ normaliza o sinal =================
          normaliza_vetor(signal, M);
          imprime_onda(signal, M);
  //================================================


          //=================== calcula a dft usando threads ===================
          int inicio=0, final = (int) M/4;
/*          cout<<"============== CALCULO DFT ===============" <<endl;

          clock_t begin_time = clock();
          thread first(calcula_dft, signal, inicio, final, M);
          thread second(calcula_dft, signal, final, final*2, M);
          thread third(calcula_dft, signal, final*2, final*3, M);
          thread fourth(calcula_dft, signal, final*3, M, M);




          first.join();
          
          second.join();
          third.join();
          fourth.join();  
          cout << float( clock () - begin_time ) /  CLOCKS_PER_SEC << "segundos" << endl;
          
          cout<<"============ FIM CALCULO DFT =============" <<endl;
*/
          //concatena arquivos
          concatena_arquivos_dft(inicio, final, final*2, final*3);

          
          std::cout <<"=========DFT INVERSA COMECO ============" << std::endl;
          clock_t begin_time = clock();
          thread inverte1(calcula_dft_inversa, signal, inicio, final, M);
          thread inverte2(calcula_dft_inversa, signal, final, final*2, M);
          thread inverte3(calcula_dft_inversa, signal, final*2, final*3, M);
          thread inverte4(calcula_dft_inversa, signal, final*3, M, M);
          

          inverte1.join();
          
          inverte2.join();
          inverte3.join();
          inverte4.join();  
          cout << float( clock () - begin_time ) /  CLOCKS_PER_SEC << "segundos" << endl;
          concatena_arquivos_inversa(inicio, final, final*2, final*3); 
  

  //================================================
  //se o sinal estiver deslocado no eixo x, encontra a media, e faz as operacoes necessárias 
          verifica_media_sinal(signal, M);
  //apos a chamada dessa funcao, o sinal *signal* está pronto p/ ser entregue pra outras funcoes


          cout<<"=============ZCR  metodo b1 ============="<<endl;
          cout<<"==            algoritmo 1              =="<<endl;
  	double *a1_b1_vector; //Feature Extract vector
  	a1_b1_vector = zcr_metodo1_b1(signal, M);
  	cout<<"==          fim algoritmo 1            =="<<endl;
  	cout<<"==                                     =="<<endl;



  //====== impressao dos valores do vetor ==========
  /*for(int i=0; fe_vector[i]!='\0'; i++)
    cout<<fe_vector[i]<<"   ";
  */


  	cout<<"==            algoritmo 2              =="<<endl;
  	double *a2_b1_vector;
  	a2_b1_vector = zcr_metodo2_b1(signal, M);
  	cout<<"==          fim algoritmo a2           =="<<endl;
  	cout<<"==                                     =="<<endl;


  	cout<<"==            algoritmo 3              =="<<endl;
  	double *a3_b1_vector;
  	a3_b1_vector = zcr_metodo3_b1(signal, M);
  	cout<<"==          fim algoritmo 3            =="<<endl;
  	cout<<"==                                     =="<<endl;
  	cout<<"============ fim metodo b1 =============="<<endl;

  	cout<<"\n\n";

    cout<<"=============== metodo b2 ==============="<<endl;
    cout<<"==            algoritmo 7              =="<<endl;
    double *a1_b2_vector; 
    a1_b2_vector = zcr_metodo1_b2(signal, M);
    cout<<"==          fim algoritmo 7            =="<<endl;
    cout<<"==                                     =="<<endl;

    cout<<"==            algoritmo 8              =="<<endl;
    double *a2_b2_vector; 
    a2_b2_vector = zcr_metodo2_b2(signal, M);
    cout<<"==          fim algoritmo 8            =="<<endl;
    cout<<"==                                     =="<<endl;

    cout<<"==            algoritmo 9              =="<<endl;
    double *a3_b2_vector; 
    a3_b2_vector = zcr_metodo3_b2(signal, M);
    cout<<"==          fim algoritmo 9            =="<<endl;
    cout<<"==                                     =="<<endl;
    cout<<"============ fim metodo b2 =============="<<endl;

    cout<<"\n\n";

    cout<<"=============== metodo b3 ==============="<<endl;
    cout<<"==            algoritmo 1              =="<<endl;
  	double *a1_b3_vector; //Feature Extract vector
   a1_b3_vector = zcr_metodo1_b3(signal, M);
   cout<<"==          fim algoritmo 1            =="<<endl;
   cout<<"============ fim metodo b3 =============="<<endl;

  //============================================================================================================
  //============================================================================================================
  //                                IMPRESSAO DOS RESULTADOS
  //ENERGIA=====================
   FILE *imprime_resultados_energia;

   imprime_resultados_energia = fopen("resultados_energia.txt", "w+");

   fprintf(imprime_resultados_energia, "         ENERGIA       \n");
   fprintf(imprime_resultados_energia, "a1                   a2                  a3\n");
   for(int i=0; fe_vector1_energia[i] != '\0'; i++)
    fprintf((imprime_resultados_energia), "%f          %f           %f\n", fe_vector1_energia[i], fe_vector2_energia[i], fe_vector3_energia[i]);

  fclose(imprime_resultados_energia);

  //ZCR=======================
  FILE *imprime_resultados_zcr;
  
  imprime_resultados_zcr = fopen("resultados_zcr_b1.txt", "w+");
  fprintf(imprime_resultados_zcr, "============== B1 =============\n");
  fprintf(imprime_resultados_zcr, "a1                   a2                      a3\n");
  for(int i=0; a1_b1_vector[i] != '\0'; i++)
    fprintf(imprime_resultados_zcr, " %f       %f       %f\n",a1_b1_vector[i], a2_b1_vector[i], a3_b1_vector[i]);
  fclose(imprime_resultados_zcr);

  imprime_resultados_zcr = fopen("resultados_zcr_b2", "w+");
  fprintf(imprime_resultados_zcr, "============== B2 =============\n");
  fprintf(imprime_resultados_zcr, "a1                   a2                      a3\n");
  for(int i=0; a1_b2_vector[i] != '\0'; i++)
    fprintf(imprime_resultados_zcr, " %.6f       %.6f       %.6f\n",a1_b2_vector[i], a2_b2_vector[i], a3_b2_vector[i]);
  fclose(imprime_resultados_zcr);


  imprime_resultados_zcr = fopen("resultados_zcr_b3", "w+");
  fprintf(imprime_resultados_zcr, "============== B3 =============\n");
  fprintf(imprime_resultados_zcr, "    a1\n");
  for(int i=0; a1_b3_vector[i] != '\0'; i++)
    fprintf(imprime_resultados_zcr, "%f\n",a1_b3_vector[i]);
  fclose(imprime_resultados_zcr);

  //============================================================================================================
  //============================================================================================================


} 

//=======================================================
//=======================================================
int verifica_media_sinal(double *signal, int M){      //  ***************FUNCIONANDO PERFEITAMENTE*******************
  using namespace std;
  double media_sinal=0;
  double variancia=0, desvio_padrao;

  for(int i=0; i<M; i++) //calcula a media do sinal
    media_sinal+= signal[i] / (double) M;
  
  

  if(media_sinal==0){ //se a media ja for 0, o sinal ja está pronto
    cout<<"a media do sinal ja' e' 0, esta pronto para a aplicacao dos metodos\n";
    return 0;
  }


  else{

    /*calculo da variancia p encontrar desvio padrao*/
    for(int i=0; i<M; i++)
      variancia += pow((signal[i]-media_sinal), 2);

    //UM POUCO DE FEEDBACK SOBRE A SITUACAO DO SINAL
    cout<<"\n\n\n==============informacoes estatisticas=============="<<endl;
    cout<<"media do sinal:"<<media_sinal<<endl;
    cout<<"variancia do sinal: "<<variancia <<endl;
    desvio_padrao = variancia/M; //calcula o desvio padrao, para que a media nao precise ser exatamente igual a 0
    cout<<"desvio padrao do sinal: "<<desvio_padrao <<endl; //imprime pra saber
    cout<<"====================================================\n\n\n"<<endl;

    for(int k=0; k<M; k++)
      signal[k] = signal[k] - media_sinal;  //subtrai a media de cada valor, de forma que a media seja 0

    media_sinal = 0; //reseta o valor da media, para que possa ser feita novamente a media do sinal
    for(int i=0; i<M; i++){ //loop pra conferir se deu msm 0. se a media der 0, é pq o valor nao foi alterado.
      media_sinal += signal[i] / (double) M;       
    }


    if(media_sinal<desvio_padrao && desvio_padrao*-1 < media_sinal){ //leva em consideração o desvio padrao
      cout<<"FIM DO PREPROCESSAMENTO"<<endl;
      return 0;
    }
  }


  // feito isso, o processo de ZCR pode ser aplicado}

  return 0;

}

//=======================================================       fim da funcao verifica media
//=======================================================


//=======================================================
//=======================================================

double * zcr_metodo1_b1(double *signal, int M){             //  ***************FUNCIONANDO PERFEITAMENTE*******************
  using namespace std;
  
  int T = (int)((100*M - L*V)/((100-V)*L)); // ~> tentativa de limpar o loop if para encontrar o erro
  int ZCR = 0; 
  int aux1 = ((int)(((100 - V)/100.0) * L)); 


  
  double *f = new double[T];
  

  for(int k=0; k<T; k++)
  {
    f[k]=0;
    for(int i = k * aux1; i < ((k * aux1) + L - 1); i++)
      f[k] += (signal[i] * signal[i + 1] < 0)?1:0;

    ZCR+=f[k];
  }


  for(int k=0; k<T; k++)
    f[k] /= (double)(ZCR); //eh esperado que aqui seja possível utilizar o vetor de caract

  return f;
}

//=======================================================
//=======================================================

double * zcr_metodo2_b1(double*signal, int M){
	

	int T = (int)((100 * M - L * V )/((100- V )*L));
	int aux1 = ((int)(((100 - V)/100.0) * L)); 

	double *f = new double[T];
	for(int k = 0; k < T; k++)
	{
		f[k]=0;
		for(int i = k * aux1; i < ((k*aux1) + L - 1); i++)
			f[k] += (signal[i] * signal[i+1] < 0)?1:0;




   f[k]/=(double)(L - 1);

 }


 return f;
}



//=======================================================
//=======================================================

double * zcr_metodo3_b1(double*signal, int M){
	

	int T = (int)((100 * M - L * V )/((100- V )*L));
	int highest_ZCR = 0;
	double *f = new double[T]; // declaracao dinamica do vetor q será retornado
	for(int k = 0; k < T; k++){ 
		f[k] = 0;
		for(int i = k * ((int)(((100 - V )/100.0) * L)); i < k * ((int)(((100 -V)/100.0) * L)) + L - 1; i++)
			f[k] +=(signal[i] * signal[i + 1] < 0)?1:0;

		if ( f [k] > highest_ZCR)
			highest_ZCR = f [k];
	}


	for(int k = 0; k < T; k++)
		f [k] /= (double)(highest_ZCR);



	return f;
}


double * zcr_metodo1_b2 (double*signal, int M)
{
	int ZCR;
	int num_primo[]={2,3,5,7,11,13,17};
  int partial_length=128;
  int tamanho_f=0;

  for(int i=0; i<(int)(sizeof(num_primo)/sizeof(int)); i++)
    tamanho_f += num_primo[i];

  double *f = new double [tamanho_f];
	int controle=0; //usado pra controlar as posicoes do vetor f

	for(int j=0; j<(int)(sizeof(num_primo)/sizeof(int)); j++)
	{
		ZCR=0;
		for (int k=0; k<num_primo[j]; k++)
		{
			partial_length = (int) (M/num_primo[j]);
			f[controle+k]=0;
			for(int i=0; i<(k*partial_length)+partial_length; i++)
				f[controle+k] += (signal[i] *signal[i+1] < 0)?1:0; 

			ZCR += f[controle+k];

		}

		controle += num_primo[j];
	}

	
	return f;
}


double *zcr_metodo2_b2(double *signal, int M)
{
	
	int num_primo[]={2,3,5,7,11,13,17};
  int partial_length=128;
  int tamanho_f=0;

  for(int i=0; i<(int)(sizeof(num_primo)/sizeof(int)); i++)
    tamanho_f += num_primo[i];

  double *f = new double [tamanho_f];
	int controle=0; //usado pra controlar as posicoes do vetor f

	for(int j=0; j<(int)(sizeof(num_primo)/sizeof(int)); j++)
	{

		for (int k=0; k<num_primo[j]; k++)
		{
			partial_length = (int) (M/num_primo[j]);
			f[controle+k]=0;
			for(int i=0; i<(k*partial_length)+partial_length; i++)
				f[controle+k] += (signal[i] *signal[i+1] < 0)?1:0; 
		}

		for(int k=0; k<num_primo[k]; k++)
			f[controle+k] /= (double) (partial_length-1);

   controle +=num_primo[j];

 }

 return f;
}

//=======================================================
//=======================================================

double *zcr_metodo3_b2(double *signal, int M)
{
	
	int num_primo[]={2,3,5,7,11,13,17};
  int partial_length=128;
  int tamanho_f=0;

  for(int i=0; i<(int)(sizeof(num_primo)/sizeof(int)); i++)
    tamanho_f += num_primo[i];

  double *f = new double [tamanho_f];
	int controle=0; //usado pra controlar as posicoes do vetor f
	int maior_zcr;


	for(int j=0; j<(int)(sizeof(num_primo)/sizeof(int)); j++)
	{
		maior_zcr=0;

		for (int k=0; k<num_primo[j]; k++)
		{
			partial_length = (int) (M/num_primo[j]);
			f[controle+k]=0;
			for(int i= (k*partial_length); i<(k*partial_length)+partial_length; i++)
				f[controle+k] += (signal[i] *signal[i+1] < 0)?1:0; 
			if(f[controle+k]>maior_zcr)
				maior_zcr = f[controle+k];

		}

		for(int k=0; k<num_primo[k]; k++)
			f[controle+k] /= (double) (maior_zcr);

   controle +=num_primo[j];

 }

 return f;
}

//=======================================================
//=======================================================

double *zcr_metodo1_b3(double* signal, int M){
	double zcr(double*, int);

  int partial_length=0;	
	double C =50; // the desired value, being 0 < C < 100
	int T = ((100/C) - ((int)(100/C)) == 0)?(100/C) - 1 : (int)(100/C);
	double * f = new double[T]; // dynamic vector declaration
	double z = zcr(&signal[0], M) * ((double)(C)/100);
	for(int k = 0; k < T; k ++){
		while(zcr(&signal[0], partial_length)<((k+1) * z))
			partial_length++;
   f[k] =(double)(partial_length)/(double)(M);
 }


 return f;

}
double zcr(double* signal, int M){
	double z = 0;
	for(int i=0; i<M-1; i++)
		z +=(signal[i] * signal[i + 1] < 0)?1:0;
	return(z);

}

//=======================================================
//=======================================================

double *energia_metodo1(double *signal, int M){
  /*L e V são definidos no inicio do codigo*/

  int T =  (int) ((100*M - L*V)/((100-V)*L));

  double E=0;
  double *f = new double[T];

  for(int k=0; k<T; k++){
    f[k] = 0;
    for(int i=k*((int) (((100-V)/100)*L)); i<k*((int) (((100-V)/100)*L))+L; i++){
      f[k] = pow(signal[i], 2);
    }

    E+= f[k];
  }

  for(int k=0; k<T; k++)
    f[k] /= E;

  return f;
}

//=======================================================
//=======================================================

double *energia_metodo2(double *signal, int M){
  double E;
  int partial_length;
  int X[] = {2, 3, 5, 7, 9, 11, 13, 17};
  int total_size_of_f=0;
  for(int i=0; i < (int)(sizeof(X)/sizeof(int)); i++)
    total_size_of_f += X[i];

  double *f = new double[total_size_of_f];
  int jump=0;

  for (int j=0; j<(int)(sizeof(X)/sizeof(int)); j++){
    E=0;
    for(int k=0; k<X[j]; k++){
      partial_length = (int)(M/X[j]);
      f[jump+k]=0;
      for(int i=(k*partial_length); i<(k*partial_length)+partial_length; i++)
        f[jump+k] += pow(signal[i], 2);
      E += f[jump+k];


    }

    for (int k=0; k<X[j]; k++)
      f[jump+k] /= E;

    jump += X[j];
  }

  return f;

}


//=======================================================
//=======================================================
double *energia_metodo3(double *signal, int M){

  int partial_length=0;
  double C=50;
  double energy(double*, int);
  int T = ((100/C) - ((int)(100/C)) ==0)?(100/C)-1 : (int)(100/C);
  double *f = new double[T];
  double z = energy(&signal[0], M)*((double)(C)/100);

  for(int k=0; k<T; k++){
    while(energy(&signal[0],partial_length) < ((k+1)*z))
      partial_length++;
    f[k] = (double)(partial_length)/(double)(M);
  }
  return f;
}
double energy(double* signal, int M){
  double e=0;
  for (int i=0; i<M; i++)
    e += pow(signal[i], 2);

  return (e);
}



//=======================================================
//=======================================================
void calcula_dft(double *signal, int inicio, int final, int M){
  double real[M], imaginario[M];
  double matriz_resposta[M][2];
  int pedaco_inicial = inicio;
  double soma_real=0, soma_imaginario=0;

  char buf[0x100];
  snprintf(buf, sizeof(buf), "dft_parte_%i.csv", pedaco_inicial);
  FILE *escreve_saida = fopen(buf, "w+");

  //inicializa os vetores com 0. A posicao [i] de ambos os vetores representam a DFT do elemento i do vetor de amostras.
  for(int i=0; i<M; i++){
    real[i] = 0;
    imaginario[i] = 0;
  }

  
  double rad, angulo;
  while(inicio < final)
  {
    for(int i=0; i<M; i++){
      angulo = ((2*180*inicio*i)/(M));
      rad = angulo *(PI/180);
      soma_real += (signal[i] * cos(rad)); //faz a soma de todos os valores reais de todas as amostras
      soma_imaginario += (-signal[i] * sin(rad)); //faz a soma de todos os valores imaginarios de todas as amostras
    }

    fprintf(escreve_saida, "%i,%f,%f\n", inicio, soma_real, soma_imaginario);

    //reseta
    soma_real=0;
    soma_imaginario=0;

    //avanca o K
    if( (inicio%1000) == 0){
      std::cout << "Thread da parte " << RED << pedaco_inicial << RESET << " em: " <<inicio << " de: " << final <<std::endl;

    }

    if(inicio == (final-1)){
      std::cout <<"========================================" << std::endl;
      std::cout << GREEN << "A THREAD da parte " << inicio << " ACABOU!" << RESET << std::endl;
      std::cout <<"========================================" << std::endl;
    }

    inicio++;
    //atualiza a tela, pra saber que esta funcionando
  }


  fclose(escreve_saida);

}
//=======================================================
//=======================================================


void calcula_dft_inversa(double *signal, int inicio, int final, int M){

  double sinal_reconstruido[M], real[M], imaginario[M];
  int pedaco_inicial = inicio;
  char buf[0x100];
  snprintf(buf, sizeof(buf), "onda_reconstruida_%i.csv", pedaco_inicial);
  FILE *escreve_saida = fopen(buf, "w+");

  FILE *entrada;
  entrada = fopen("dft_total.csv", "r");

  int linha_arquivo =0, posicao;
  char lixo1, lixo2;

  //loop para importar todos os valores da dft p/ dentro do programa
  while( (fscanf(entrada, "%d %c %lf %c %lf\n", &posicao, &lixo1, &real[linha_arquivo], &lixo2, &imaginario[linha_arquivo])) != EOF){
    //std::cout<<"lendo arquivo, linha: "<< linha_arquivo <<std::endl;
    linha_arquivo++;
  }
  //inicializa o sinal reconstruido com 0's
  std::cout<<"fim da leitura do arquivo "<< std::endl;
  double rad, angulo, dist1, dist2, dist3, dist4, total1, total2;
  long double soma_real=0, soma_imaginario=0;
  while(inicio<final){
    for(int i=0; i<M; i++){
      angulo = ((2*180*inicio*i)/(M));
      rad = angulo *(PI/180);
      //printf("para o k=%i, o angulo sera: %lf e o valor da transformada usada e %lf real %lf img\n", inicio, angulo, real[i], imaginario[i]);
      
      dist1 = (real[i] * cos(rad));
      dist2 = (real[i] * sin(rad));
      dist3 = (imaginario[i] * cos(rad));
      dist4 = -(imaginario[i] * sin(rad));
      total1 = dist1 + dist4;
      total2 = dist2 + dist3;      

      soma_real += total1;
      soma_imaginario += total2; //faz a soma de todos os valores imaginarios de todas as amostras

    }
    sinal_reconstruido[inicio] = (soma_real + soma_imaginario)/M;
    fprintf(escreve_saida, "%lf\n", sinal_reconstruido[inicio]);
    soma_real=0;
    soma_imaginario=0;
    if( (inicio%1000) == 0){
      std::cout << BLUE << "RECONSTRUINDO SINAL" << RESET << " - Thread da parte " << RED << pedaco_inicial << RESET << " em: " <<inicio << " de: " << final <<std::endl;

    }

    if(inicio == (final-1)){
      std::cout <<"========================================" << std::endl;
      std::cout << GREEN << "A THREAD da parte " << inicio << " ACABOU!" << RESET << std::endl;
      std::cout <<"========================================" << std::endl;
    }
    inicio++;
    
  }

  
  fclose(escreve_saida);

}
//=======================================================
//=======================================================

void imprime_onda(double *signal, int M){
  FILE *escreve_saida;

  escreve_saida = fopen("dados_onda.txt", "w+");

  for(int i=0; i<M; i++){
    fprintf(escreve_saida, "%f\n", signal[i]);

  }

  fclose(escreve_saida);  
}

//=======================================================
//=======================================================
//-------------------------------------------------------------

void escala_log(double *signal, int M)
{
  FILE *entrada, *saida;
  entrada = fopen("dft_total.csv", "r");
  saida = fopen("dft_transformada.csv", "w+");
  
  char lixo1, lixo2;
  double real, imaginario, resultado_log_real, resultado_log_imaginario;
  int posicao;
  fprintf(saida, "real,imaginario\n");
  while( (fscanf(entrada, "%d %c %lf %c %lf\n", &posicao, &lixo1, &real, &lixo2, &imaginario))!= EOF){
    if(imaginario<0 && real>0){
      resultado_log_imaginario = log10(-imaginario);
      fprintf(saida, "%lf,%lf\n", log10(real), (-1 * resultado_log_imaginario));
    }

    if(imaginario>0 && real<0){
      resultado_log_real = log10(-real);
      fprintf(saida, "%lf,%lf\n", (-1 * resultado_log_real), log10(imaginario));
    }

    if(imaginario<0 && real<0){
      resultado_log_real = log10(-real);
      resultado_log_imaginario = log10(-imaginario);
      fprintf(saida, "%lf,%lf\n", (-1 * resultado_log_real),(-1 * resultado_log_imaginario));
    }

    if (imaginario>0 && real>0){
      fprintf(saida, "%lf,%lf\n",log10(real),log10(imaginario)); 
    }
    
    


  }

  fclose(entrada);
  fclose(saida);
}


void normaliza_vetor(double *signal, int M){
  double maior_valor = signal[0];

  for(int i=1; i<M; i++)
  {
    if(fabs(signal[i]) > maior_valor)
      maior_valor = fabs(signal[i]);

  }

  for(int i=0; i<M; i++){
    signal[i] /= maior_valor;
  }



}


void concatena_arquivos_dft(int var1, int var2, int var3, int var4){
  FILE *entrada, *saida;

  int vetor[4], controle=0, posicao;
  double real_dft, imaginario_dft;
  char lixo1, lixo2;
  char buf[0x100];
  //=== atribui os pedacos iniciais das threads em um vetor para iniciar a concatenacao
  vetor[0]=var1, vetor[1]=var2, vetor[2]=var3, vetor[3]=var4;
  
  

  //===== concatena os 4 arquivos gerados pelas threadsem calcula_dft

  saida = fopen("dft_total.csv", "w+");
  while(controle<4){
    //fprintf(escreve_saida, "%i,%f,%f\n", inicio, soma_real, soma_imaginario);
    snprintf(buf, sizeof(buf), "dft_parte_%i.csv", vetor[controle]);
    entrada = fopen(buf, "r");
    while( (fscanf(entrada, "%d %c %lf %c %lf\n", &posicao, &lixo1, &real_dft, &lixo2, &imaginario_dft)) != EOF){
      fprintf(saida, "%i,%lf,%lf\n", posicao, real_dft, imaginario_dft);
      
    }
    controle++;
  }

  fclose(entrada);
  fclose(saida);
}

void concatena_arquivos_inversa(int var1, int var2, int var3, int var4){
 FILE *entrada, *saida;

 int vetor[4], controle=0;
 double onda_reconstruida;
 char buf[0x100];


  //=== atribui os pedacos iniciais das threads em um vetor para iniciar a concatenacao
 vetor[0]=var1, vetor[1]=var2, vetor[2]=var3, vetor[3]=var4;
 
 saida = fopen("onda_reconstruida_total.csv", "w+");
 while(controle<4){
  snprintf(buf, sizeof(buf), "onda_reconstruida_%i.csv", vetor[controle]);
  entrada = fopen(buf, "r");
  while( (fscanf(entrada, "%lf\n", &onda_reconstruida)) != EOF ){
    fprintf(saida, "%lf\n", onda_reconstruida);

  }
  controle++;
}//===== nesse ponto é esperado que os arquivos das threads já tenham sido concatenados 
  //===== no arquivo onda_reconstruida_total.
fclose(entrada);
fclose(saida);


}



void calcula_espectro_potencia(double *signal, int M, int comprimento_espectro){

  int  inicio=0, final=comprimento_espectro, linha_matriz=0, coluna_matriz=0;
  int quantidade_janelas = (M-comprimento_espectro)/(comprimento_espectro/2) + 1 ;
  double espectro_total[quantidade_janelas][comprimento_espectro][2];
  double angulo, rad, soma_real=0, soma_imaginario=0;
  double media_espectro[quantidade_janelas][2];

  std::cout<<BLUE<<"Espectro total - linhas: " <<quantidade_janelas << " - colunas: "<< comprimento_espectro << RESET << std::endl;
  std::cout <<"qnt de janelas" <<quantidade_janelas << std::endl;

  espectro_intervalo:
  while(inicio < final)
  {
    for(int i=inicio; i<final; i++){
      angulo = ((2*180*inicio*i)/(comprimento_espectro));
      rad = angulo *(PI/180);
      soma_real += (signal[i] * cos(rad)); //faz a soma de todos os valores reais de todas as amostras
      soma_imaginario += (-signal[i] * sin(rad)); //faz a soma de todos os valores imaginarios de todas as amostras
    }

    espectro_total[linha_matriz][coluna_matriz][0] = soma_real;
    espectro_total[linha_matriz][coluna_matriz][1] = soma_imaginario;

    soma_real=0;
    soma_imaginario=0;
    coluna_matriz++;
    inicio++;

  }
  if( (linha_matriz == (quantidade_janelas-1))  || final > (comprimento_espectro * quantidade_janelas)) {
    goto continua_execucao;
  }
  else{

    inicio = final - comprimento_espectro/2;
    final = inicio + comprimento_espectro;
    linha_matriz++;
    coluna_matriz=0;
    std::cout <<"linha: " << linha_matriz << ", inicio: " << inicio <<", final: " <<final<<std::endl;
    goto espectro_intervalo;
  }



  continua_execucao:
  std::cout<<"inicializacao da matriz de media dos espectros" << std::endl;
  for(int i=0; i<quantidade_janelas; i++){
    media_espectro[i][0]=0;
    media_espectro[i][1]=0;
  }

  for(int i=0; i<quantidade_janelas; i++){
    for(int j=0; j<comprimento_espectro; j++){
      media_espectro[i][0] += espectro_total[i][j][0]/comprimento_espectro;
      media_espectro[i][1] += espectro_total[i][j][1]/comprimento_espectro;

    }
  }

  FILE *saida;

  saida = fopen("espectro_potencia.csv", "w+");
  fprintf(saida, "real,imaginario\n");

  for(int i=0; i<quantidade_janelas; i++){
    fprintf(saida, "%lf,%lf\n", media_espectro[i][0],media_espectro[i][1]);
  }



}


//=======================================================
//=======================================================
